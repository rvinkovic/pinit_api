<?php
/**
 * Created by PhpStorm.
 * User: Antonio
 * Date: 11.2.2016.
 * Time: 21:46
 */

use Phalcon\Di;
use Phalcon\Di\FactoryDefault;

ini_set('display_errors',1);
error_reporting(E_ALL);

define('ROOT_PATH', __DIR__);
define('PATH_LIBRARY', __DIR__ . '/../app/library/');
define('PATH_SERVICES', __DIR__ . '/../app/services/');
define('PATH_RESOURCES', __DIR__ . '/../app/resources/');

set_include_path(
    ROOT_PATH . PATH_SEPARATOR . get_include_path()
);

// Required for phalcon/incubator
include __DIR__ . "/../vendor/autoload.php";

// Use the application autoloader to autoload the classes
// Autoload the dependencies found in composer
$loader = new \Phalcon\Loader();

$loader->registerDirs(
    array(
        ROOT_PATH,
	__DIR__
    )
);

$loader->register();

$di = new FactoryDefault();
Di::reset();

// Add any needed services to the DI here

$config = require (__DIR__ .'/../app/config/config.php');

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$dbConfig = $config->database->toArray();
$adapter = $dbConfig['adapter'];
unset($dbConfig['adapter']);

$class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

$connection = new $class($dbConfig);

/** And the database is cleaned before first run. */
$sql = "DELETE FROM items;
        DELETE FROM labels;
        DELETE FROM notes;
        DELETE FROM users_notes;
        DELETE from geos";

$connection->connect();
$result = $connection->query($sql);
if (!$result){
    throw new Exception("Failed to delete test database!");
} else {
    echo "Truncated database!";
}

Di::setDefault($di);
