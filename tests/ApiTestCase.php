<?php

/**
 * Created by PhpStorm.
 * User: Antonio
 * Date: 11.2.2016.
 * Time: 23:30
 */
class ApiTestCase extends UnitTestCase
{

    const INVALID_KEY = "ffffffff-ffff-ffff-ffff-ffffffffffff";
    const VALID_KEY = "1c707780-a981-4bf2-b752-9472dccefbf6";

    protected $client;

    public function setUp(){
        parent::setup();
        $this->client= new GuzzleHttp\Client([
                'base_uri' => 'http://46.101.151.125',
                'http_errors' => false
            ]);
    }

    protected function readRequestData($current_function, $current_class){
        return "./fixtures/" . strtolower($current_class) . "/" . strtolower($current_function) . ".json";
    }

    protected function readResponseData($current_function, $current_class){
        return "./fixtures/" . strtolower($current_class) . "/" . strtolower($current_function) . "_response.json";
    }

}
