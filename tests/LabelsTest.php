<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 12.2.2016.
 * Time: 16:01
 */
class LabelsTest extends ApiTestCase
{
    public function test_Get_InvalidKey_Labels(){
        $response = $this->client->get('/api/labels', [
            'query' => [
                'api_key' => parent::INVALID_KEY
            ]
        ]);

        $this->assertEquals(403, $response->getStatusCode());
    }

    public function test_Get_ValidKey_Labels(){
        $response = $this->client->get('/api/labels', [
            'query' => [
                'api_key' => parent::VALID_KEY
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getBody(), true);

        $this->assertArrayHasKey('labels', $data);
    }

    public function test_Post_ValidKey_Labels(){

        $body = file_get_contents($this->readRequestData(__FUNCTION__,__CLASS__));
        $response = $this->client->post('/api/labels', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('labels', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__,__CLASS__),
            (string)$response->getBody()
        );
    }

    /**
     * @depends test_Post_ValidKey_Labels
     */
    public function test_Post_DuplicateId_Labels(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__,__CLASS__));
        $response = $this->client->post('/api/labels', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('conflicts', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__,__CLASS__),
            (string)$response->getBody()
        );
    }

    public function test_Post_InvalidData_Labels(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__,__CLASS__));
        $response = $this->client->post('/api/labels', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('errors', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__,__CLASS__),
            (string)$response->getBody()
        );
    }

    /**
     * @depends test_Post_ValidKey_Labels
     */
    public function test_Put_ValidKey_Labels(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__,__CLASS__));
        $response = $this->client->put('/api/labels', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('labels', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__,__CLASS__),
            (string)$response->getBody()
        );
    }

    public function test_Put_InvalidId_Labels(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__,__CLASS__));
        $response = $this->client->put('/api/labels', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('errors', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__,__CLASS__),
            (string)$response->getBody()
        );
    }

    /**
     * @depends test_Post_ValidKey_Labels
     */
    public function test_Put_InvalidData_Labels(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__,__CLASS__));
        $response = $this->client->put('/api/labels', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('errors', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__,__CLASS__),
            (string)$response->getBody()
        );
    }

    /**
     * @depends test_Put_ValidKey_Labels
     */
    public function test_Delete_ValidId_Labels(){
        $response = $this->client->delete('/api/label', [
            'query' => [
                'api_key' => self::VALID_KEY,
                'uuid' => '3ffd35a3edaf44499a552284ff70adf3'
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__,__CLASS__),
            (string)$response->getBody()
        );
    }

    public function test_Delete_InvalidId_Labels(){
        $response = $this->client->delete('/api/label', [
            'query' => [
                'api_key' => self::VALID_KEY,
                'uuid' => self::INVALID_KEY
            ]
        ]);

        $this->assertEquals(410, $response->getStatusCode());
    }
}