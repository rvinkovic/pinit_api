<?php

use Phalcon\Mvc\Router;

// These routes simulate real URIs
$testRoutes = array(
    '/api/users',
    '/api/notes',
    '/api/items',
    '/api/labels',
    '/api/archive/notes',
    '/api/archive/notes/todos',
    '/api/users/notes',
);

$router = new Router();

// Add here your custom routes
// ...

// Testing each route
foreach ($testRoutes as $testRoute) {

    // Handle the route
    $router->handle($testRoute);

    echo 'Testing ', $testRoute, '<br>';

    // Check if some route was matched
    if ($router->wasMatched()) {
        echo 'Controller: ', $router->getControllerName(), '<br>';
        echo 'Action: ', $router->getActionName(), '<br>';
    } else {
        echo 'The route wasn\'t matched by any route<br>';
    }

    echo '<br>';
}