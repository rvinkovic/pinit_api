<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 12.2.2016.
 * Time: 16:32
 */
class ItemsTest extends ApiTestCase
{

    public function test_Get_InvalidKey_Items(){
        $response = $this->client->get('/api/items', [
            'query' => [
                'api_key' => parent::INVALID_KEY
            ]
        ]);

        $this->assertEquals(403, $response->getStatusCode());
    }

    public function test_Get_ValidKey_Items(){
        $response = $this->client->get('/api/items', [
            'query' => [
                'api_key' => parent::VALID_KEY
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getBody(), true);

        $this->assertArrayHasKey('items', $data);
    }

    public function test_Post_ValidKey_Items(){

        $body = file_get_contents($this->readRequestData(__FUNCTION__, __CLASS__));
        $response = $this->client->post('/api/items', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('items', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__, __CLASS__),
            (string)$response->getBody()
        );
    }

    /**
     * @depends test_Post_ValidKey_Items
     */
    public function test_Post_DuplicateId_Items(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__, __CLASS__));
        $response = $this->client->post('/api/items', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('conflicts', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__, __CLASS__),
            (string)$response->getBody()
        );
    }

    public function test_Post_InvalidData_Items(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__, __CLASS__));
        $response = $this->client->post('/api/items', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('errors', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__, __CLASS__),
            (string)$response->getBody()
        );
    }

    /**
     * @depends test_Post_ValidKey_Items
     */
    public function test_Put_ValidKey_Items(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__, __CLASS__));
        $response = $this->client->put('/api/items', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('items', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__, __CLASS__),
            (string)$response->getBody()
        );
    }

    public function test_Put_InvalidId_Items(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__, __CLASS__));
        $response = $this->client->put('/api/items', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('errors', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__, __CLASS__),
            (string)$response->getBody()
        );
    }

    /**
     * @depends test_Post_ValidKey_Items
     */
    public function test_Put_InvalidData_Items(){
        $body = file_get_contents($this->readRequestData(__FUNCTION__, __CLASS__));
        $response = $this->client->put('/api/items', [
            'query' => [
                'api_key' => self::VALID_KEY
            ],
            'json' => json_decode($body)
        ]);
        $this->assertEquals(200, $response->getStatusCode());

        $data = $response->getBody();

        $this->assertArrayHasKey('errors', json_decode($data, true));
        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__, __CLASS__),
            (string)$response->getBody()
        );
    }

    /**
     * @depends test_Put_ValidKey_Items
     */
    public function test_Delete_ValidId_Items(){
        $response = $this->client->delete('/api/item', [
            'query' => [
                'api_key' => self::VALID_KEY,
                'uuid' => '3ffd35a3edaf44499a552284ff70adf3'
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertJsonStringEqualsJsonFile($this->readResponseData(__FUNCTION__, __CLASS__),
            (string)$response->getBody()
        );
    }

    public function test_Delete_InvalidId_Items(){
        $response = $this->client->delete('/api/item', [
            'query' => [
                'api_key' => self::VALID_KEY,
                'uuid' => self::INVALID_KEY
            ]
        ]);

        $this->assertEquals(410, $response->getStatusCode());
    }
}