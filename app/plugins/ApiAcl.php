<?php
/**
 * Created by PhpStorm.
 * User: Antonio
 * Date: 11.2.2016.
 * Time: 16:33
 */

use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl as PhalconAcl;
use Phalcon\DI;

/**
 * ApiAcl
 */
class ApiAcl extends Plugin
{
    /**
     * @param DI $dependencyInjector
     */
    public function __construct(DI $dependencyInjector)
    {
        $this->_dependencyInjector = $dependencyInjector;
    }

    /**
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function getAcl()
    {
        if (!isset($this->persistent->apiAcl)) {
            $acl = new \Phalcon\Acl\Adapter\Memory();
            $acl->setDefaultAction(PhalconAcl::DENY);

            $roles = array(
                'apiUser' => new \Phalcon\Acl\Role('apiUser'),
                'anonymousUser' => new \Phalcon\Acl\Role('anonymousUser')
            );
            foreach ($roles as $role) {
                $acl->addRole($role);
            }

            $apiUserResources = array(
                'geos' => array('index','list','create','update','delete'),
                'items' => array('index','list','create','update','delete'),
                'labels' => array('index','list','create','update','delete'),
                'notes' => array('index','list','create','update','delete'),
                'users' => array('index','list','create','update','delete'),
                'usersnotes' => array('index','list','create','update','delete')
            );

            foreach ($apiUserResources as $resource => $actions) {
                $acl->addResource(new \Phalcon\Acl\Resource($resource), $actions);
            }

            foreach ($apiUserResources as $resource => $actions) {
                foreach ($actions as $action){
                    $acl->allow('apiUser', $resource, $action);
                }
            }

            $anonymousUserResources = array(
                'users' => array('login', 'create')
            );

            foreach ($anonymousUserResources as $resource => $actions) {
                $acl->addResource(new \Phalcon\Acl\Resource($resource), $actions);
            }

            foreach ($anonymousUserResources as $resource => $actions) {
                foreach ($actions as $action){
                    $acl->allow('anonymousUser', $resource, $action);
                }
            }

            //The acl is stored in session, APC would be useful here too
            $this->persistent->apiAcl = $acl;
        }

        return $this->persistent->apiAcl;
    }

    /**
     * @param Event $event
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        $api_key = $this->request->getQuery('api_key');

        // Below you should check your database
        $account = null;

        $user = Users::findFirst(["api_key=:api_key:",'bind'=>['api_key'=>$api_key]]);

        if ($user){
            $account = array(
                'roles' => array('apiUser')
            );
        } else {
            $account = array(
                'roles' => array('anonymousUser')
            );
        }
        // End of database call, assuming 'account' is an array OR null

        if (!is_array($account) || !array_key_exists('roles', $account)) {
            $this->response->setStatusCode(403, 'Forbidden');
            $this->response->send();
            return false;
        }

        $roles = array();
        if (is_array($account['roles'])) {
            $roles = $account['roles'];
        }

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $acl = $this->getAcl();

        foreach ($roles as $singleRole) {
            if ($acl->isAllowed($singleRole, $controller, $action) == PhalconAcl::ALLOW) {
                return true;
            }
        }

        $this->response->setStatusCode(403, 'Forbidden');
        $this->response->send();
        return false;
    }
}