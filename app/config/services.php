<?php
/**
 * Services are globally registered in this file
 *
 * @var \Phalcon\Config $config
 */

use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;

use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Manager as EventsManager;



/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component. We disable it as we're echoing everything from controllers.
 */
$di->setShared('view', function () use ($config) {

    $view = new View();

    $view->disable();

    return $view;
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () use ($config) {
    $dbConfig = $config->database->toArray();
    $adapter = $dbConfig['adapter'];
    unset($dbConfig['adapter']);

    $class = 'Phalcon\Db\Adapter\Pdo\\' . $adapter;

    return new $class($dbConfig);
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Register the session flash service with the Twitter Bootstrap classes
 */
$di->set('flash', function () {
    return new Flash(array(
        'error'   => 'alert alert-danger',
        'success' => 'alert alert-success',
        'notice'  => 'alert alert-info',
        'warning' => 'alert alert-warning'
    ));
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/**
 * Registering a dispatcher. This is used for API authentication.
 */
$di->set('dispatcher', function() use ($di) {

    $dispatcher = new Dispatcher();
    $eventsManager = $di->getShared('eventsManager');

    // Begin ACL
    $security = new ApiAcl($di);
    $eventsManager->attach('dispatch', $security);
    // End ACL
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
});


/**
 * Register a router
 */
$di->set('router', function(){
	require __DIR__ . '/routes.php';
	
	return $router;
});

$di->set("response", function (){
    $response=new Phalcon\Http\Response();
    $response->setContentType("application/json");
    return $response;
});
