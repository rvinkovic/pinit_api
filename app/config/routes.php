<?php

use Phalcon\Mvc\Router;

// Create the router
$router = new Router();

// USERS
//get userdata with username and password
$router->addPost(
    "/api/login",
    array(
        "controller" => "users",
        "action"     => "login"
    )
);
$router->addGet(
    "/api/user",
    array(
        "controller" => "users",
        "action"     => "getUser"
    )
);
//get all users(basic data)
$router->addGet(
    "/api/users",
    array(
        "controller" => "users",
        "action"     => "list"
    )
);


$router->addPost(
    "/api/users",
    array(
        "controller" => "users",
        "action"     => "create"
    )
);

$router->addPut(
    "/api/user",
    array(
        "controller" => "users",
        "action"     => "update",
		"username"   => 1
    )
);


$router->addDelete(
    "/api/user",
    array(
        "controller" => "users",
        "action"     => "delete"
    )
);

//NOTES
$router->addGet(
    "/api/notes",
    array(
        "controller" => "notes",
        "action"     => "list"
    )
);


$router->addPost(
    "/api/notes",
    array(
        "controller" => "notes",
        "action"     => "create"
    )
);

$router->addPut(
    "/api/notes",
    array(
        "controller" => "notes",
        "action"     => "update"
    )
);


$router->addDelete(
    "/api/note",
    array(
        "controller" => "notes",
        "action"     => "delete"
    )
);

//ITEMS
$router->addGet(
    "/api/items",
    array(
        "controller" => "items",
        "action"     => "list"
    )
);


$router->addPost(
    "/api/items",
    array(
        "controller" => "items",
        "action"     => "create"
    )
);

$router->addPut(
    "/api/items",
    array(
        "controller" => "items",
        "action"     => "update"
    )
);


$router->addDelete(
    "/api/item",
    array(
        "controller" => "items",
        "action"     => "delete"
    )
);

//Labels
$router->addGet(
    "/api/labels",
    array(
        "controller" => "labels",
        "action"     => "list"
    )
);


$router->addPost(
    "/api/labels",
    array(
        "controller" => "labels",
        "action"     => "create"
    )
);

$router->addPut(
    "/api/labels",
    array(
        "controller" => "labels",
        "action"     => "update"
    )
);


$router->addDelete(
    "/api/label",
    array(
        "controller" => "labels",
        "action"     => "delete"
    )
);

//Geos
$router->addGet(
    "/api/geos",
    array(
        "controller" => "geos",
        "action"     => "list"
    )
);


$router->addPost(
    "/api/geos",
    array(
        "controller" => "geos",
        "action"     => "create"
    )
);

$router->addPut(
    "/api/geos",
    array(
        "controller" => "geos",
        "action"     => "update"
    )
);


$router->addDelete(
    "/api/geo",
    array(
        "controller" => "geos",
        "action"     => "delete"
    )
);

//ARCHIVE
$router->addGet(
    "/api/archive/todos",
    array(
        "controller" => "archive",
        "action"     => "list"
    )
);

$router->addDelete(
    "/api/archive/todos",
    array(
        "controller" => "archive",
        "action"     => "delete"
    )
);
//ne znam el radi tako sa (id)
$router->addDelete(
    "/api/archive/todos/:id",
    array(
        "controller" => "archive",
        "action"     => "delete",
		"id"         => 1
    )
);
//PERMISIONS(useres_notes table)
$router->addGet(
    "/api/users/notes",
    array(
        "controller" => "usersnotes",
        "action"     => "list"
    )
);
$router->addPost(
    "/api/users/notes",
    array(
        "controller" => "usersnotes",
        "action"     => "create"
    )
);
$router->addPut(
    "/api/users/notes",
    array(
        "controller" => "usersnotes",
        "action"     => "update"
    )
);
//brisanje podjeljenih biljeski
$router->addDelete(
    "/api/users/notes",
    array(
        "controller" => "usersnotes",
        "action"     => "delete"
    )
);

$router->notFound(
    array(
        "controller" => "index",
        "action"     => "route404"
    )
);