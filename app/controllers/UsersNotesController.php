<?php

class UsersNotesController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }
    public function listAction(){

        $api_key = $this->request->getQuery("api_key");

        $users_notes= new UsersNotes();
        $user=Users::findFirst("api_key='$api_key'");
        if($user){
            $result=UsersNotes::find("users_api_key='$api_key'");
            $result=json_encode(array('users_notes' => $result->toArray()));
            echo $result;
            $this->response->setStatusCode(200, "OK");


        }
        else{
            $this->response->setStatusCode(404, "NOT FOUND");
        }

    }

    public function createAction()
    {
        $api_key = $this->request->getQuery("api_key");
        $recieved = $this->request->getJsonRawBody(true);


        $user= Users::findFirst("api_key='$api_key'");
        if ($user) {
            $users_notes=$recieved["users_notes"];
            foreach ($users_notes as $field){
                $user_note = new UsersNotes();
                $form=new UsersNotesForm();
                $form->bind($field,$users_notes);
                if ($form->isValid()) {
                    try {
                        $user_note->save();
                        $saved[]=$user_note->notes_uuid;
                    } catch (PDOException $e) {
                        //save failed
                        //$errors[] = $note->uuid;
                    }
                } else {
                    //form not valid
                    //$errors[] = $note->uuid;
                }
            }

        } else {
            $this->response->setStatusCode(404, "NOT FOUND");

        }
        //ispis uuid sejvanih
        echo json_encode(["saved" => $saved]);
    }
    public function deleteAction(){
        $api_key = $this->request->getQuery("api_key");
        $note_uuid=$this->request->getQuery("uuid");
        $response = new \Phalcon\Http\Response();
        $response->setContentType("application/json");

        $user= Users::findFirst("api_key='$api_key'");
        //ako postoji user s tim api keyem
        if($user){
            $note= UsersNotes::findFirst(
                array(
                    "users_api_key" => $api_key,
                    "uuid"=>$note_uuid
                )
            );
            //ako label postoji
            if($note){
                $note->setDeleted(1);
                try{
                    $note->save();
                    $response->setContent("Successful");
                    $response->setStatusCode(200, "OK");
                    $response->send();
                }
                catch(Exception $e){
                    $response->setContent(json_encode($e->getMessage()));
                    $response->setStatusCode(409, "CONFLICT");
                    $response->send();
                }
            }

        }
        //ako label ne postoji

        else{
            $response->setStatusCode(404, "NOT FOUND");
            $response->send();
        }



    }

    public function updateAction(){
        $api_key = $this->request->getPut("api_key");
        $users_notes=$this->request->getPut("users_notes");
        $response = new \Phalcon\Http\Response();
        $response->setContentType("application/json");

        $user=Users::findFirst("api_key='$api_key'");
        if($user){
            foreach ( $users_notes as $row){


                $user_note= Labels::findFirst(
                    array(
                        "user_email" => $api_key,
                        "uuid"=>$row['notes_uuid']
                    )
                );



                $user_note->setName($row['user_email']);
                $user_note->setUuid($row['notes_uuid']);
                $user_note->setCanEdit($row['can_edit']);
                $user_note->setDeleted($row['can_delete']);
                $user_note->setOwner($row['owner']);
                $user_note->setDateUpdated($row['date_updated']);


                try{
                    $user_note->save();
                }
                catch (Exception $e){
                    $errors[] = $e->getMessage;
                }
            }
            $response->setContent($errors);
            //koji status kod
            $response->send();

        }



        else{
            $response->setStatusCode(404, "NOT FOUND");
            $response->send();
        }




    }

}

