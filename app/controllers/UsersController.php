<?php
use Phalcon\Validation;
class UsersController extends \Phalcon\Mvc\Controller

{

    public function indexAction()
    {

    }

    public function loginAction()
    {
        $received = $this->request->getJsonRawBody(true);
        $email = $received['email'];
        $password = $received['password'];
        $user = Users::findFirst("email='$email'");
        if($user && $user->deleted==0){
            if ($this->security->checkHash($password, $user->password)) {
                $data=json_encode(array(
                    'deleted' =>  $user->deleted,
                    'email' =>  $user->email,
                    'name' =>  $user->name,
                    'surname' =>   $user->surname,
                    'picture' =>   $user->picture,
                    'username' => $user->username,
                    'api_key'  => $user->api_key
                ));
                echo $data;
                $this->response->setStatusCode(200, "OK");
            } else {
                $this->response->setStatusCode(403, "FORBIDDEN");
            }
        }

        else{
            $this->response->setStatusCode(404, "NOT FOUND");
        }
    }
    //getALl users
    public function listAction(){

        $result=Users::find();
        echo (json_encode(array('users' => $result->toArray())));
        $this->response->setStatusCode(200, "OK");

    }
    //get user information based with username and password
    public function getUserAction(){

        $email=$this->request->get("email");
        $user = Users::findFirst("email='$email'");
        if($user){
            $data=json_encode(array(
                'deleted' =>  $user->deleted,
                'email' =>  $user->email,
                'name' =>  $user->name,
                'surname' =>   $user->surname,
                'picture' =>   $user->picture,
                'username' => $user->username
            ));
            echo $data;
           $this->response->setStatusCode(200, "OK");
            }

        else{

          $this->response->setStatusCode(404, "NOT FOUND");
      }



    }
    public function createAction()
    {
            //POTREBNO RASPAKIRATI ZAHTJEV DA SE NE SALJE U PLAIN TEXT
            $user = new Users();
            //$user->populate($this->request->getPost());

            //var_dump(($this->request->getJsonRawBody(true)));
            $recieved=$this->request->getJsonRawBody(true);

            $form=new UsersForm($user);
            $form->bind($recieved,$user);
            // var_dump($user->name);
            //var_dump($user->password);
            //if form is valid
            if($form->isValid()){
                //generate username
                $username=strtok($recieved["email"], '@');
                $user->username=$username;
                $random=new \Phalcon\Security\Random();
                $api_key=$random->uuid();
                //var_dump($api_key);
                $user->api_key=$api_key;
                $user->deleted=0;
                $user->password=$this->security->hash($recieved['password']);
                try {
                    $user->save();
                    //ispis greški
                   // var_dump($user->getMessages());
                    $data=json_encode(array(
                        'deleted' =>  $user->deleted,
                        'email' =>  $user->email,
                        'name' =>  $user->name,
                        'surname' =>   $user->surname,
                        'picture' =>   $user->picture,
                        'username' => $user->username,
                        'api_key' => $user->api_key
                    ));
                    echo $data;
                    $this->response->setStatusCode(200, "OK");
                }
                catch(PDOException $e){
                    echo json_encode(["errors"=> ["Email or username is already in use"]]);
                    $this->response->setStatusCode(400, "Bad Request");
                }
                catch(Exception $e){
                   echo json_encode(["errors"=> $e->getMessage()]);
                   $this->response->setStatusCode(400, "Bad Request");
               }
            }
            //form not valid
            else{
                $msgs=[];
                $i=0;
                foreach ($form->getMessages() as $message) {
                     $msgs[]=$message->getMessage();
                }
                echo json_encode(["errors"=> $msgs]);
                $this->response->setStatusCode(400, "Bad Request");
            }


        }

    public function deleteAction(){

        $api_key=$this->request->getQuery("api_key");
        $user = Users::findFirst("api_key='$api_key'");

        if($user){
            $user->deleted=1;
            try{
                $user->save();
                $this->response->setStatusCode(200, "OK");;
            }
            catch(PDOException $e){
                $this->response->setStatusCode(409, "CONFLICT");
            }
        }

    }

    public function updateAction()
    {

        $api_key = $this->request->getQuery("api_key");
        $recieved = $this->request->getJsonRawBody(true);
        $user = Users::findFirst("api_key='$api_key'");

        $form = new UsersUpdateForm();
        $form->bind($recieved, $user);
        if ($form->isValid()) {
            try {
                $user->save();
                echo json_encode($user->toArray());
                $this->response->setStatusCode(200, "OK");
            } catch (PDOException $e) {

                $this->response->setStatusCode(409, "CONFLICT");
            }
        }//form not valid
        else {
            echo json_encode($user->toArray());
            $this->response->setStatusCode(400, "Bad Request");
        }

    }
}

