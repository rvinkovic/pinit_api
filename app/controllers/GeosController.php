<?php

class GeosController extends ControllerBase
{

    public function indexAction()
    {

    }

    public function listAction()
    {
        //ispis geosa od usera, treba joinati notes sa users_notes po api_key
        $api_key = $this->request->getQuery('api_key');
        $query = $this->modelsManager->createQuery(
            "SELECT Geos.* from Geos JOIN Notes ON Notes.geos_id = Geos.uuid
            where Notes.uuid IN (SELECT notes_uuid from UsersNotes WHERE users_api_key=:api_key:)");
        $result = $query->execute(
            array(
                'api_key' => $api_key
            )
        );

        echo json_encode(array('geos' => $result->toArray()));
        $this->response->setStatusCode(200, "OK");
    }

    public function createAction(){
        $recieved = $this->request->getJsonRawBody(true);
        $saved = [];
        $conflict = [];
        $errors=[];

        $geos = $recieved["geos"];

        foreach ($geos as $field) {
            $geo = new Geos();
            $form = new GeosForm();
            $form->bind($field, $geo);
            if ($form->isValid()) {
                try {
                    $geo->save();
                    $saved[] = $geo->toJsonArray();
                } catch (PDOException $e) {
                    if($e->errorInfo[1] == parent::DB_LAYER_ERROR_DUPLICATE) {
                        //save failed vrati iz baze s tim uuid(konflikt sa istim idom)
                        $failed = Geos::findFirst(["uuid=:uuid:", 'bind' => ['uuid' => $geo->uuid]]);
                        $conflict[] = $failed->toJsonArray();
                    }
                }
            } else {
                //form not valid
                $errors[$geo->uuid] = $form->getMessagesArray();
            }
        }
        //ispis uuid sejvanih
        echo json_encode(["geos" => $saved, "conflicts" => $conflict, "errors"=>$errors]);
    }

    public function deleteAction(){
        $uuid = $this->request->getQuery('uuid');
        $geo = Geos::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$uuid]]);
        if(!$geo){
            $this->response->setStatusCode(410, "GONE");
            return;
        }
        try {
            $geo->deleted = 1;
            $geo->save();
            echo json_encode($geo->toJsonArray());
            $this->response->setStatusCode(200, "OK");
        } catch(Exception $e) {
            /* TODO why send conflict here? It probably isn't one... */
            $this->response->setStatusCode(409, "CONFLICT");
        }

    }

    public function updateAction(){
        $recieved = $this->request->getJsonRawBody(true);
        $geos = $recieved["geos"];
        $saved=[];
        $conflict=[];
        $errors=[];

        foreach ($geos as $row) {
            //ako ne postoji geo
            $geo = Geos::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$row['uuid']]]);
            if(!$geo){
                $errors[$row['uuid']] = ["Not Found."];
                continue;
            }

            $form = new GeosForm();
            $form->bind($row, $geo);
            if ($form->isValid()) {
                try {
                    $geo->save();
                    $saved[] = $geo->toJsonArray();
                } catch (PDOException $e) {
                    //save failed vrati iz baze s tim uuid(konflikt sa istim idom)
                    /* TODO detect constraint failures and ignore them */
                    $failed = Geos::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$geo->uuid]]);
                    $conflict[] = $failed->toJsonArray();
                }
            } else {
                //form not valid
                $errors[$geo->uuid] = $form->getMessagesArray();
            }
        }

        //ispis uuid sejvanih
        echo json_encode(["geos" => $saved, "conflicts" => $conflict, "errors"=>$errors]);
    }

}

