<?php

class UsersNotesController extends ControllerBase
{

    public function indexAction()
    {

    }
    public function listAction(){

        $api_key = $this->request->getQuery("api_key");

        $result=UsersNotes::find("users_api_key='$api_key'");
        echo json_encode(array('users_notes' => $result->toArray()));
        $this->response->setStatusCode(200, "OK");

    }

    public function createAction(){
        $recieved = $this->request->getJsonRawBody(true);

        $saved = [];
        $conflict = [];
        $errors=[];

        $users_notes=$recieved["users_notes"];

        foreach ($users_notes as $field){
            $user_note = new UsersNotes();
            $form=new UsersNotesForm();
            $form->bind($field,$user_note);
            if ($form->isValid()) {
                try {
                    $user_note->save();
                    $saved[]=$user_note->toJsonArray();
                } catch (PDOException $e) {
                    //save failed vrati iz baze s tim uuid(konflikt sa istim idom)
                    if($e->errorInfo[1] == parent::DB_LAYER_ERROR_DUPLICATE) {
                        //save failed vrati iz baze s tim uuid(konflikt sa istim idom)
                        $failed = UsersNotes::findFirst([
                            "conditions" => "users_api_key=:uuid: AND notes_uuid=:notes_uuid:",
                            'bind'=>['uuid'=>$field['users_api_key'], 'notes_uuid'=>$field['notes_uuid']]
                        ]);
                        var_dump($failed);
                        $conflict[] = $failed->toJsonArray();
                    } else {
                        //form not valid
                        //TODO think of a proper way to return errors for this
                        $errors[$user_note->notes_uuid] = $e->getMessage();
                    }
                }
            } else {
                //form not valid
                //TODO think of a proper way to return errors for this
                $errors[$user_note->notes_uuid] = $form->getMessagesArray();
            }
        }

        //ispis uuid sejvanih
        echo json_encode(["users_notes" => $saved, "conflicts" => $conflict, "errors"=>$errors]);
    }

    public function deleteAction(){
        $api_key = $this->request->getQuery("api_key");
        $note_uuid = $this->request->getQuery("uuid");

        $note = UsersNotes::findFirst(
            array(
                "users_api_key" => $api_key,
                "uuid"=>$note_uuid
            )
        );
        if(!$note){
            $this->response->setStatusCode(410, "GONE");
            return;
        }

        //ako usernote postoji

        try{
            if($note){
                $note->setDeleted(1);
                    $note->save();
                    echo json_encode($note->toArray());
                    $this->response->setStatusCode(200, "OK");
            }
        } catch(Exception $e){
            /* TODO why send conflict here? It probably isn't one... */
            $this->response->setStatusCode(409, "CONFLICT");
        }

    }

    public function updateAction(){
        $users_notes=$this->request->getPut("users_notes");

        $saved=[];
        $conflict=[];
        $errors=[];

        foreach ($users_notes as $row){

            $user_note= UsersNotes::findFirst(["users_api_key=:api_key:", "notes_uuid=:notes_uuid:",
            'bind'=>['api_key'=>$row['users_api_key'], 'notes_uuid'=>$row['notes_uuid']]]);

            $form=new UsersNotesForm();
            $form->bind($row, $users_notes);

            if($form->isValid()){
                try {
                    $user_note->save();
                    $saved[] = $user_note->toJsonArray();
                } catch (PDOException $e){
                    $failed = UsersNotes::findFirst(["users_api_key=:api_key:", "notes_uuid=:notes_uuid:",
                        'bind'=>['api_key'=>$row['users_api_key'], 'notes_uuid'=>$row['notes_uuid']]]);
                    $conflict[] = $failed->toArray();
                }
            } else {
                //form not valid
                //TODO think of a way to send errors properly
                $errors[$user_note->notes_uuid] = $form->getMessagesArray();
            }
        }
        //ispis uuid sejvanih
        echo json_encode(["users_notes" => $saved, "conflicts" => $conflict, "errors"=>$errors]);
    }

}

