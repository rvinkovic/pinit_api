<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    const DB_LAYER_ERROR_DUPLICATE = 1062;
}
