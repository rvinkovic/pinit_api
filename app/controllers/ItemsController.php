<?php
use Phalcon\Mvc\Model\Query;
class ItemsController extends ControllerBase
{

    public function indexAction(){

    }

    public function listAction(){
        $api_key = $this->request->getQuery("api_key");
        $query = $this->modelsManager->createQuery("SELECT * from Items where notes_id IN(SELECT notes_uuid from UsersNotes where users_api_key=:api_key:)");
        $result  = $query->execute(
            array(
                'api_key' => $api_key
            )
        );

        echo json_encode(array('items' => $result->toArray()));
        $this->response->setStatusCode(200, "OK");
    }

    public function createAction(){
        $recieved = $this->request->getJsonRawBody(true);
        $saved = [];
        $conflict = [];
        $errors=[];

        $items = $recieved["items"];

        foreach ($items as $field) {
            $item = new Items();
            $form = new ItemsForm();
            $form->bind($field, $item);
            if ($form->isValid()) {
                try {
                    $item->save();
                    $saved[] = $item->toJsonArray();
                } catch (PDOException $e) {
                    if($e->errorInfo[1] == parent::DB_LAYER_ERROR_DUPLICATE) {
                        //save failed vrati iz baze s tim uuid(konflikt sa istim idom)
                        $failed = Items::findFirst(["uuid=:uuid:", 'bind' => ['uuid' => $item->uuid]]);
                        $conflict[] = $failed->toJsonArray();
                    } else {
                        $errors[$item->uuid] = $e->getMessage();
                    }
                }
            } else {
                //form not valid
                $errors[$item->uuid] = $form->getMessagesArray();
            }
        }
        //ispis uuid sejvanih
        echo json_encode(["items" => $saved, "conflicts" => $conflict, "errors"=>$errors]);
    }

    public function deleteAction(){
        $uuid = $this->request->getQuery('uuid');
        $item = Items::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$uuid]]);
	    if(!$item){
	    	$this->response->setStatusCode(410, "GONE");
		    return;
	    }
        try {
            $item->deleted = 1;
            $item->save();
            echo json_encode($item->toJsonArray());
            $this->response->setStatusCode(200, "OK");
        } catch(Exception $e) {
            /* TODO why send conflict here? It probably isn't one... */
            $this->response->setStatusCode(409, "CONFLICT");
        }

    }

    public function updateAction(){
        $recieved = $this->request->getJsonRawBody(true);
        $items = $recieved["items"];
        $saved=[];
        $conflict=[];
        $errors=[];

        foreach ($items as $row) {
            //ako ne postoji note
            $item = Items::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$row['uuid']]]);
            if(!$item){
                $errors[$row['uuid']] = ["Not Found."];
                continue;
            }

            $form = new ItemsForm();
            $form->bind($row, $item);
            if ($form->isValid()) {
                try {
                    $item->save();
                    $saved[] = $item->toJsonArray();
                } catch (PDOException $e) {
                    //save failed vrati iz baze s tim uuid(konflikt sa istim idom)
                    /* TODO detect constraint failures and ignore them */
                    $failed = Items::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$item->uuid]]);
                    $conflict[] = $failed->toJsonArray();
                }
            } else {
                //form not valid
                $errors[$item->uuid] = $form->getMessagesArray();
            }
        }

        //ispis uuid sejvanih
        echo json_encode(["items" => $saved, "conflicts" => $conflict, "errors"=>$errors]);
    }

}

