<?php

class LabelsController extends ControllerBase
{

    public function indexAction()
    {

    }

    public function listAction()
    {
        $api_key = $this->request->getQuery("api_key");
        $result = Labels::find(["users_api_key=:api_key:", 'bind'=>['api_key'=>$api_key]]);
        echo json_encode(array('labels' => $result->toArray()));
        $this->response->setStatusCode(200, "OK");
    }

    public function createAction()
    {
        $recieved = $this->request->getJsonRawBody(true);

        $saved = [];
        $conflict = [];
        $errors = [];

        if (!array_key_exists("labels", $recieved)){
            $this->response->setStatusCode(404, "Bad Request");
        }

        $labels = $recieved["labels"];
        foreach ($labels as $field) {
            $label = new Labels();
            $form = new LabelsForm();
            $form->bind($field, $label);
            if ($form->isValid()) {
                try {
                    $label->save();
                    $saved[] = $label->toJsonArray();
                } catch (PDOException $e) {
                    if($e->errorInfo[1] == parent::DB_LAYER_ERROR_DUPLICATE) {
                        $failed = Labels::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$field['uuid']]]);
                        $conflict[] = $failed->toJsonArray();
                    } else {
                        $errors[$label->uuid] = $e->getMessage();
                    }
                }
            } else {
                //form not valid
                $errors[$label->uuid] = $form->getMessagesArray();
            }
        }
        //u svakom slucaju ispiši error
        echo json_encode(["labels" => $saved, "conflicts" => $conflict, "errors"=>$errors]);
    }

    public function deleteAction()
    {
        $uuid = $this->request->getQuery('uuid');
        $label = Labels::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$uuid]]);
        if(!$label){
            $this->response->setStatusCode(410, "GONE");
            return;
        }
        try {
            $label->deleted = 1;
            $label->save();
            echo json_encode($label->toJsonArray());
            $this->response->setStatusCode(200, "OK");
        } catch(Exception $e) {
            /* TODO why send conflict here? It probably isn't one... */
            $this->response->setStatusCode(409, "CONFLICT");
        }
    }

    public function updateAction(){
        $recieved = $this->request->getJsonRawBody(true);

        if (!array_key_exists("labels", $recieved)){
            $this->response->setStatusCode(404, "Bad Request");
        }

        $labels = $recieved["labels"];

        $saved=[];
        $conflict=[];
        $errors=[];

        foreach ($labels as $row) {
            //ako ne postoji label
            $label = Labels::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$row['uuid']]]);
            if(!$label){
                $errors[$row['uuid']] = ["Not Found."];
                continue;
            }

            $form = new LabelsForm();
            $form->bind($row, $label);
            if ($form->isValid()) {
                try {
                    $label->save();
                    $saved[] = $label->toJsonArray();
                } catch (PDOException $e) {
                    //TODO detektirati constraint failure i ako je to ignorirati
                    //save failed vrati iz baze s tim uuid(konflikt sa istim idom)
                    $failed = Labels::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$row['uuid']]]);
                    $conflict[] = $failed->toJsonArray();
                }
            } else {
                //form not valid
                $errors[$label->uuid] = $form->getMessagesArray();
            }
        }
        //ispis uuid sejvanih
        echo json_encode(["labels" => $saved, "conflicts" => $conflict, "errors"=>$errors]);

    }
}

