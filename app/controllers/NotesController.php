<?php


class NotesController extends \Phalcon\Mvc\Controller
{

    public function indexAction()
    {

    }

    public function listAction()
    {
        //ispis biljezaka od usera, treba joinati notes sa users_notes po api_key
        $api_key = $this->request->getQuery('api_key');
        $result = $this->modelsManager->createBuilder()
            ->from('Notes')
            ->join("UsersNotes", "UsersNotes.notes_uuid=Notes.uuid")
            ->where("users_api_key='$api_key'")->getQuery()->execute();//and date_updated>timestamp DODATI
        echo json_encode(array('notes' => $result->toArray()));

        $this->response->setStatusCode(200, "OK");
    }

    //  POST /api/notes/
    public function createAction()
    {
        $recieved = $this->request->getJsonRawBody(true);
        $saved = [];
        $conflict = [];
        $errors=[];

        $notes = $recieved["notes"];
        foreach ($notes as $field) {
            $note = new Notes();
            $form = new NotesForm();
            $form->bind($field, $note);
            if ($form->isValid()) {
                try {
                    $note->save();
                    $saved[] = $note->toJsonArray();
                } catch (PDOException $e) {
                    //todo this is actually vendor specific error code, try to find a general solution
                    if($e->errorInfo[1] == 1062){
                        //save failed vrati iz baze s tim uuid(konflikt sa istim idom)
                        $failed = Notes::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$note->uuid]]);
                        $conflict[] = $failed->toJsonArray();
                    } else {
                        $errors[$note->uuid] = $note->getMessages();
                    }
                }
            } else {
                //form not valid
                $errors[$note->uuid] = $form->getMessagesArray();
            }
        }

        //ispis uuid sejvanih
        echo json_encode(["notes" => $saved, "conflicts" => $conflict, "errors"=>$errors]);
    }

    public function deleteAction()
    {
        $uuid = $this->request->getQuery('uuid');
        $note = Notes::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$uuid]]);
        if(!$note){
            $this->response->setStatusCode(410, "GONE");
            return;
        }
	    try {
            $note->status = 3;
            $note->save();
            echo json_encode($note->toJsonArray());
            $this->response->setStatusCode(200, "OK");
        } catch(Exception $e) {
            /* TODO why send conflict here? It probably isn't one... */
            $this->response->setStatusCode(409, "CONFLICT");
        }
    }

    public function updateAction()
    {
        $recieved = $this->request->getJsonRawBody(true);
        $notes = $recieved["notes"];
        $saved=[];
        $conflict=[];
        $errors=[];

        foreach ($notes as $row) {
            //ako ne postoji note
            $note = Notes::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$row['uuid']]]);
            if(!$note){
                $errors[$row['uuid']] = array("Not Found.");
                continue;
            }

            $form = new NotesForm();
            $form->bind($row, $note);
            if ($form->isValid()) {
                try {
                    $note->save();
                    $saved[] = $note->toJsonArray();
                } catch (PDOException $e) {
                    //save failed vrati iz baze s tim uuid(konflikt sa istim idom)
                    $failed = Notes::findFirst(["uuid=:uuid:", 'bind'=>['uuid'=>$row['uuid']]]);
                    $conflict[] = $failed->toJsonArray();
                }
            } else {
                //form not valid
                $errors[$note->uuid] = $form->getMessagesArray();
            }
        }
        //ispis uuid sejvanih
        echo json_encode(["notes" => $saved, "conflicts" => $conflict, "errors"=>$errors]);
    }

}

