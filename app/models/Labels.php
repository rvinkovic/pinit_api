<?php

class Labels extends PinitModel
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $date_updated;

    /**
     *
     * @var integer
     */
    protected $deleted;

    /**
     *
     * @var string
     */
    protected $uuid;

    /**
     *
     * @var string
     */
    protected $users_api_key;

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('user_email', 'Users', 'email', array('alias' => 'Users'));
        $this->belongsTo('notes_id', 'Notes', 'id', array('alias' => 'Notes'));
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'labels';
    }

}
