<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 27.1.2016.
 * Time: 10:50
 */
class PinitModel extends \Phalcon\Mvc\Model
{
    public function populate(array $post){
        foreach($post as $x =>$y){
            $this->$x=$y;
        }

    }
    public function __set($name, $value){

        $this->$name=$value;
    }

    public function __get($name){

        return $this->$name;
    }

    public function toJsonArray(){
        $array = $this->toArray();
        unset($array['id']);
        return $array;
    }

}