<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class Users extends PinitModel
{

    /**
     *
     * @var string
     */
    protected $api_key;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $name;

    /**
     *
     * @var string
     */
    protected $surname;

    /**
     *
     * @var string
     */
    protected $password;

    /**
     *
     * @var string
     */
    protected $picture;

    /**
     *
     * @var integer
     */
    protected $deleted;

    /**
     *
     * @var string
     */
    protected $username;



    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('email', 'Labels', 'user_email', array('alias' => 'Labels'));
        $this->hasMany('email', 'UsersNotes', 'user_email', array('alias' => 'UsersNotes'));
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'users';
    }

}
