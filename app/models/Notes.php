<?php

class Notes extends PinitModel
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $title;

    /**
     *
     * @var string
     */
    protected $date_created;

    /**
     *
     * @var string
     */
    protected $date_updated;

    /**
     *
     * @var string
     */
    protected $date_alarm;

    /**
     *
     * @var string
     */
    protected $text;

    /**
     *
     * @var integer
     */
    protected $priority;

    /**
     *
     * @var string
     */
    protected $color;

    /**
     *
     * @var integer
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $uuid;

    /**
     *
     * @var integer
     */
    protected $type;

    /**
     *
     * @var string
     */
    protected $labels_id;

    /**
     *
     * @var string
     */
    protected $geos_id;



    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->hasMany('id', 'Geos', 'notes_id', array('alias' => 'Geos'));
        $this->hasMany('id', 'Items', 'notes_id', array('alias' => 'Items'));
        $this->hasMany('id', 'Labels', 'notes_id', array('alias' => 'Labels'));
        $this->hasMany('id', 'UsersNotes', 'notes_id', array('alias' => 'UsersNotes'));
    }


    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'notes';
    }

}
