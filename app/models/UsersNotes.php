<?php

class UsersNotes extends PinitModel
{

    /**
     *
     * @var integer
     */
    protected $notes_id;

    /**
     *
     * @var string
     */
    protected $api_key;

    /**
     *
     * @var integer
     */
    protected $can_edit;

    /**
     *
     * @var integer
     */
    protected $can_delete;

    /**
     *
     * @var integer
     */
    protected $owner;

    /**
     *
     * @var string
     */
    protected $date_updated;

    /**
     *
     * @var integer
     */
    protected $deleted;

    /**
     *
     * @var string
     */
    protected $notes_uuid;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('user_email', 'Users', 'email', array('alias' => 'Users'));
        $this->belongsTo('notes_id', 'Notes', 'id', array('alias' => 'Notes'));
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'users_notes';
    }

}
