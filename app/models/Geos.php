<?php

class Geos extends PinitModel
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $address;

    /**
     *
     * @var double
     */
    protected $longitude;

    /**
     *
     * @var double
     */
    protected $latitude;

    /**
     *
     * @var integer
     */
    protected $range;

    /**
     *
     * @var string
     */
    protected $date_updated;

    /**
     *
     * @var string
     */
    protected $uuid;



    public function initialize()
    {
        $this->belongsTo('notes_id', 'Notes', 'id', array('alias' => 'Notes'));
    }



    public function getSource()
    {
        return 'geos';
    }

}
