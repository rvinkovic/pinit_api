<?php

class Items extends PinitModel
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $text;

    /**
     *
     * @var integer
     */
    protected $checked;

    /**
     *
     * @var string
     */
    protected $date_updated;

    /**
     *
     * @var string
     */
    protected $uuid;

    /**
     *
     * @var integer
     */
    protected $deleted;

    /**
     *
     * @var string
     */
    protected $notes_id;


    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->belongsTo('notes_id', 'Notes', 'id', array('alias' => 'Notes'));
    }


    public function getSource()
    {
        return 'items';
    }

}
