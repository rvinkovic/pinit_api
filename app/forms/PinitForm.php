<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 9.2.2016.
 * Time: 12:09
 */
class PinitForm extends \Phalcon\Forms\Form
{

    public function getMessagesArray(){
        $messages = [];
        foreach ($this->getMessages() as $message) {
            $messages[] = $message->getMessage();
        }
        return $messages;
    }
}