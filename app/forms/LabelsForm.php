<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 5.2.2016.
 * Time: 14:43
 */
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Forms\Element\Date;
class LabelsForm extends PinitForm
{

    public function initialize(){
        $this->nameField();
        $this->dateUpdatedField();
        $this->deletedField();
        $this->uuidField();
        $this->apiKeyField();

    }



    private function nameField(){
        $field=new Text("name");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Name is required.'
            ])
        ]);

        $this->add($field);
    }

    private function dateUpdatedField(){
        $field=new Numeric ("date_updated");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Date updated is required.'
            ])
        ]);

        $this->add($field);
    }

    private function deletedField(){
        $field=new Numeric("deleted");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Deleted is required.'
            ])
        ]);

        $this->add($field);
    }

    private function uuidField(){
        $field=new Text("uuid");
        $field->addValidators([
            new PresenceOf([
                'message' => 'uuid is required.'
            ])
        ]);
        $this->add($field);
    }
    private function apiKeyField(){
        $field=new Text("users_api_key");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Users api key is required.'
            ])
        ]);
        $this->add($field);


    }




}