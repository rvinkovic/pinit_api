<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 6.2.2016.
 * Time: 16:13
 */
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Forms\Element\Date;
class UsersNotesForm extends PinitForm{
    public function initialize(){
        //not mandatory data without validation

        $this->usersApiKeyField();
        $this->notesUuidField();
        $this->canEditField();
        $this->canDeleteField();
        $this->ownerField();
        $this->dateUpdatedField();
        $this->deletedField();

    }
    private function usersApiKeyField(){
        $field=new Text("users_api_key");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Api key  is required.'
            ])
        ]);
        $this->add($field);
    }
    private function notesUuidField(){
        $field=new Text("notes_uuid");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Notes uuid   is required.'
            ])
        ]);
        $this->add($field);
    }

    private function canEditField(){
        $field=new Numeric("can_edit");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Can edit is required.'
            ])
        ]);
        $this->add($field);
    }
    private function canDeleteField(){
        $field=new Numeric("can_delete");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Can delete is required.'
            ])
        ]);
        $this->add($field);
    }

    private function ownerField(){
        $field=new Numeric("owner");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Owner is required.'
            ])
        ]);
        $this->add($field);
    }

    private function dateUpdatedField(){
        $field=new Date("date_updated");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Date updated is required.'
            ])
        ]);

        $this->add($field);
    }
    private function deletedField(){
        $field=new Numeric("deleted");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Deleted is required.'
            ])
        ]);
        $this->add($field);
    }


}