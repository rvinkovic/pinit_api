<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 9.2.2016.
 * Time: 11:19
 */
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Forms\Element\Date;
class ItemsForm extends PinitForm
{
    public function initialize(){
        $this->textField();
        $this->checkedField();
        $this->dateUpdatedField();
        $this->deletedField();
        $this->uuidField();
        $this->notesIdField();

    }



    private function textField(){
        $field=new Text("text");
        $this->add($field);
        $field->addValidators([
            new PresenceOf([
                'message' => 'Text is required.'
            ])
        ]);
    }

    private function checkedField(){
        $field=new Numeric("checked");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Checked is required.'
            ])
        ]);

        $this->add($field);
    }

    private function dateUpdatedField(){
        $field=new Numeric("date_updated");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Date is required.'
            ])
        ]);

        $this->add($field);
    }

    private function uuidField(){
        $field=new Text("uuid");
        $field->addValidators([
            new PresenceOf([
                'message' => 'uuid is required.'
            ])
        ]);
        $this->add($field);
    }
    private function deletedField(){
        $field=new Numeric("deleted");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Deleted is required.'
            ])
        ]);

        $this->add($field);
    }
    private function notesIdField(){
        $field=new Text("notes_id");
        $this->add($field);
        $field->addValidators([
            new PresenceOf([
                'message' => 'NotesID is required.'
            ])
        ]);
    }


}