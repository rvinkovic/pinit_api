<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 9.2.2016.
 * Time: 11:07
 */
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Forms\Element\Date;
class GeosForm extends PinitForm
{
    public function initialize(){
        $this->addressField();
        $this->longitudeField();
        $this->latitudeField();
        $this->rangeField();
        $this->uuidField();
        $this->dateUpdatedField();
        $this->deletedField();
    }



    private function addressField(){
        $field=new Text("address");
        $this->add($field);
    }

    private function longitudeField(){
        $field=new Text("longitude");
        $field->setFilters(array('float'));
        $field->addValidators([
            new PresenceOf([
                'message' => 'Longitude is required.'
            ])
        ]);

        $this->add($field);
    }

    private function latitudeField(){
        $field=new Text("latitude");
        $field->setFilters(array('float'));
        $field->addValidators([
            new PresenceOf([
                'message' => 'Latitude is required.'
            ])
        ]);

        $this->add($field);
    }

    private function uuidField(){
        $field=new Text("uuid");
        $field->addValidators([
            new PresenceOf([
                'message' => 'uuid is required.'
            ])
        ]);
        $this->add($field);
    }

    private function rangeField(){
        $field=new Numeric("range");

        $this->add($field);
    }

    private function dateUpdatedField(){
        $field=new Numeric ("date_updated");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Date updated is required.'
            ])
        ]);

        $this->add($field);
    }

    private function deletedField(){
        $field=new Numeric ("deleted");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Deleted is required.'
            ])
        ]);

        $this->add($field);
    }


}