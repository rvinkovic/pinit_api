<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 9.2.2016.
 * Time: 14:30
 */
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;
class UsersUpdateForm extends PinitForm
{
    public function initialize(){
        $this->emailField();
        $this->nameField();
        $this->surnameField();
        $this->pictureField();

    }

    private function emailField(){
        $field=new Email("email");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Email is required.'
            ]),
            new EmailValidator([
                'message' => 'Email format is invalid.'
            ])
        ]);
        $this->add($field);

    }

    private function nameField(){
        $field=new Text("name");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Name is required.'
            ])
        ]);

        $this->add($field);
    }

    private function surnameField(){
        $field=new Text("surname");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Surname is required.'
            ])
        ]);

        $this->add($field);
    }

    private function pictureField(){
        $field=new Text("picture");


        $this->add($field);
    }
}