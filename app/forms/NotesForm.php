<?php

/**
 * Created by PhpStorm.
 * User: vinko
 * Date: 5.2.2016.
 * Time: 14:43
 */
use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Forms\Element\Date;
class NotesForm extends PinitForm
{

    public function initialize(){
        //not mandatory data without validation

        $this->titleField();
        $this->dateCreatedField();
        $this->dateUpdatedField();
        $this->dateAlarmField();
        $this->textField();
        $this->priorityField();
        $this->colorField();
        $this->statusField();
        $this->uuidField();
        $this->typeField();
        $this->labelsIdField();
        $this->geosIdField();


    }


    private function titleField(){
        $field=new Text("title");
        $this->add($field);
    }
    private function dateCreatedField(){
        $field=new Numeric("date_created");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Date created is required.'
            ])
        ]);

        $this->add($field);
    }

    private function dateUpdatedField(){
        $field=new Numeric("date_updated");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Date updated is required.'
            ])
        ]);

        $this->add($field);
    }
    private function dateAlarmField(){
        $field=new Numeric("date_alarm");

        $this->add($field);
    }
    private function textField(){
        $field=new Text("text");
        $this->add($field);
    }
    private function priorityField(){
        $field=new Numeric("priority");
        $this->add($field);
    }
    private function colorField(){
        $field=new Text("color");
        $this->add($field);
    }
    private function statusField(){
        $field=new Numeric("status");
        $field->addValidators([
            new PresenceOf([
                'message' => 'Status is required.'
            ])
        ]);
        $this->add($field);
    }



    private function uuidField(){
        $field=new Text("uuid");
        $field->addValidators([
            new PresenceOf([
                'message' => 'uuid is required.'
            ])
        ]);
        $this->add($field);
    }

    private function typeField(){
        $field=new Numeric("type");
        $field->addValidators([
            new PresenceOf([
                'message' => 'type is required.'
            ])
        ]);
        $this->add($field);
    }

    private function labelsIdField(){
        $field=new Text("labels_id");
        $this->add($field);


    }
    private function geosIdField(){
        $field=new Text("geos_id");
        $this->add($field);


    }



}